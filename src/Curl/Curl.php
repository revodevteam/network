<?php
namespace Framework\Network\Curl;

class Curl
{

    protected $curl;

    public function __construct()
    {
        $this->curl = curl_init();
        $this->returnValue();
        $this->timeout();
        $this->strict();
    }

    public function setUrl(string $url)
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);

        return $this;
    }

    public function header(array $headers)
    {
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
        return $this;
    }

    public function timeout($time = 30)
    {
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $time);
        return $this;
    }

    public function cookie($cookie)
    {
        if (is_array($cookie)) {
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $cookie);
        } elseif (is_string($cookie)) {
            curl_setopt($this->curl, CURLOPT_COOKIE, $cookie);
        } else {
            throw new \Exception('Cookie value must be string or array');
        }

        return $this;
    }

    public function cookieJar($cookieJar)
    {

        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $cookieJar);

        return $this;
    }

    public function cookieFile($cookieFile)
    {

        curl_setopt($this->curl, CURLOPT_COOKIEFILE, $cookieFile);

        return $this;
    }

    public function post($postData)
    {

        curl_setopt($this->curl, CURLOPT_POST, true);

        if (is_array($postData)) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($postData));
        } elseif (is_string($postData)) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($postData))
            );
        }

        return $this;
    }

    public function put($data)
    {
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($data));

        return $this;
    }

    public function head()
    {
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'HEAD');

        return $this;
    }

    public function delete()
    {
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'DELETE');

        return $this;
    }

    public function get()
    {
        curl_setopt($this->curl, CURLOPT_HTTPGET, true);

        return $this;
    }

    public function returnValue($return = true)
    {

        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, $return);

        return $this;
    }

    public function certInfo($varify = true)
    {
        curl_setopt($this->curl, CURLOPT_CERTINFO, $varify);
        return $this;
    }

    public function userAgent($agent)
    {
        curl_setopt($this->curl, CURLOPT_USERAGENT, $agent);
        return $this;
    }

    public function referer($referer)
    {
        curl_setopt($this->curl, CURLOPT_REFERER, $referer);
        return $this;
    }

    public function followLocation($status = true)
    {
        curl_setopt($this->curl, FOLLOWLOCATION, $status);
        return $this;
    }

    public function strict($status = true)
    {
        curl_setopt($this->curl, CURLOPT_FAILONERROR, $status);
        return $this;
    }

    public function sslON($caPath)
    {
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($this->curl, CURLOPT_CAINFO, $caPath);
        return $this;
    }

    public function output()
    {
        $response = curl_exec($this->curl);

        if (curl_errno($this->curl) !== 0) {
            throw new CurlException(curl_error($this->curl), curl_getinfo($this->curl, CURLINFO_HTTP_CODE));
        }

        return $response;
    }

    public function ping()
    {
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, false);
        return $this->output();
    }

    public function debug($path)
    {
        curl_setopt($this->curl, CURLOPT_STDERR, $path);
        return $this;
    }

    public function headerReturn()
    {
        return curl_getinfo($this->curl, CURLINFO_HEADER_OUT);
    }

    public function error()
    {
        return curl_error($this->curl);
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }

    public function close()
    {
        curl_close($this->curl);

        return $this;
    }
}
